<?php
/*
Plugin Name: Engegroup
Plugin URI: https://engegroup.com
Description: Custom post types and Custom post fields for Engegroup
Author: Black Magenta
Version: 1.0.0.0
Author URI: https://www.blackmagenta.com.br/
Text Domain: engegroup
*/

/**
 * Require Cmb Field Select2 type
 */
require 'cmb-field-select2.php';

/**
 * Load Translation
 */
function engegroup_load_textdomain() 
{
    load_muplugin_textdomain('engegroup', basename(dirname(__FILE__)) . '/languages');
}
add_action('plugins_loaded', 'engegroup_load_textdomain');

/**
 * Show Engegroup Segments
 * Return a array of engegroup_segments custom post type
 */
function show_engegroup_segments() {
    if (post_type_exists('engegroup_segments')) {
        $args = array(
            'posts_per_page'   => '-1',
            'orderby'          => 'title',
            'order'            => 'ASC',
            'post_type'        => 'engegroup_segments',
            'post_status'      => 'publish',
            'suppress_filters' => true,
            'fields'           => '',
        );
        $segments_posts = get_posts( $args );

        $segments = array();
        foreach ($segments_posts as $segment) {
            $segments[$segment->ID] = $segment->post_title;
        }

        if (!empty($segments)) {
            return $segments;
        } else {
            return array(
                'none' => __('No segments found', 'engegroup'),
            );
        }

    } else {
        return array(
            'none' => __('No segments type', 'engegroup'),
        );
    } 
}

/**
 * Get a list of posts
 *
 * Generic function to return an array of posts formatted for CMB2. Simply pass
 * in your WP_Query arguments and get back a beautifully formatted CMB2 options
 * array.
 *
 * @param array $query_args WP_Query arguments
 * @return array CMB2 options array
 */
function get_cmb_segments_array( $query_args = array() ) {
	$defaults = array(
        'posts_per_page' => -1,
        'orderby'          => 'title',
        'order'            => 'ASC',
        'post_status'      => 'publish',
	);
	$query = new WP_Query( array_replace_recursive( $defaults, $query_args ) );
	return wp_list_pluck( $query->get_posts(), 'post_title', 'post_title' );
}

/**
 *
 * Register post types
 *
 */

/**
 * Segments
 */
function cpt_engegroup_segments() 
{
    $labels = array(
            'name'                  => __('Segments', 'engegroup'),
            'singular_name'         => __('Segment', 'engegroup'),
            'menu_name'             => __('Segments', 'engegroup'),
            'name_admin_bar'        => __('Segment', 'engegroup'),
            'archives'              => __('Segment Archives', 'engegroup'),
            'attributes'            => __('Segment Attributes', 'engegroup'),
            'parent_item_colon'     => __('Parent Segment:', 'engegroup'),
            'all_items'             => __('All Segments', 'engegroup'),
            'add_new_item'          => __('Add New Segment', 'engegroup'),
            'add_new'               => __('Add segment', 'engegroup'),
            'new_item'              => __('New Segment', 'engegroup'),
            'edit_item'             => __('Edit Segment', 'engegroup'),
            'update_item'           => __('Update Segment', 'engegroup'),
            'view_item'             => __('View Segment', 'engegroup'),
            'view_items'            => __('View Segments', 'engegroup'),
            'search_items'          => __('Search Segment', 'engegroup'),
            'not_found'             => __('Not found', 'engegroup'),
            'not_found_in_trash'    => __('Not found in Trash', 'engegroup'),
            'featured_image'        => __('Featured Image', 'engegroup'),
            'set_featured_image'    => __('Set featured image', 'engegroup'),
            'remove_featured_image' => __('Remove featured image', 'engegroup'),
            'use_featured_image'    => __('Use as featured image', 'engegroup'),
            'insert_into_item'      => __('Insert into segment', 'engegroup'),
            'uploaded_to_this_item' => __('Uploaded to this segment', 'engegroup'),
            'items_list'            => __('Segments list', 'engegroup'),
            'items_list_navigation' => __('Segments list navigation', 'engegroup'),
            'filter_items_list'     => __('Filter Segments list', 'engegroup'),
    );
    $rewrite = array(
            'slug'                  => __('segments', 'engegroup'),
            'with_front'            => false,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Segment', 'engegroup'),
            'description'           => __('Engegroup Segments', 'engegroup'),
            'labels'                => $labels,
            'supports'              => array('title', 'page-attributes'),
            'taxonomies'            => array(''),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47 60"><g fill="none" fill-rule="evenodd"><path fill="black" d="M24.318 2.397c7.884 0 12.156.832 13.243 1.3v4.759H11.07v-4.76c1.087-.467 5.36-1.299 13.247-1.299zm12.072 8.459l-7.026 9.57H3.567l7.024-9.57H36.39zm-4.048 9.57l6.307-8.59 5.045 8.59H32.342zm-1.17 23.004c2.124-.64 4.573-1.274 6.846-1.274 2.63 0 4.723.83 6.572 2.61V57.55H31.172V43.43zm0-2.498V22.827H44.59v18.867c-1.904-1.286-4.06-1.91-6.568-1.91-1.938 0-4.064.354-6.85 1.148zm-2.398 3.256v13.365H2.397V46.089a25.035 25.035 0 0 0 9.496 1.827c5.612 0 10.904-1.751 15.575-3.296l1.306-.432zM6.8 30.808a1.2 1.2 0 0 0 1.2-1.2v-6.78h20.774v18.84c-.682.219-1.369.443-2.06.671-4.46 1.477-9.517 3.151-14.796 3.151-3.364 0-6.479-.66-9.518-2.02V22.828h3.2v6.78c0 .663.539 1.2 1.2 1.2zm40.144-9.482a1.595 1.595 0 0 0-.035-.108L39.957 9.33V3.247C39.957 1.093 34.695 0 24.317 0 13.938 0 8.675 1.093 8.675 3.247v6.165L.237 20.913c-.047.061-.075.12-.11.192l-.041.08A1.37 1.37 0 0 0 0 21.63v37.12c0 .662.538 1.2 1.2 1.2h44.59c.662 0 1.2-.538 1.2-1.2V21.325h-.046z"/><path fill="black" d="M5.597 36.783a1.2 1.2 0 1 0 2.4 0v-1.246a1.2 1.2 0 1 0-2.4 0v1.246z"/></g></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('engegroup_segments', $args); 
} 
add_action('init', 'cpt_engegroup_segments', 0);

/**
 * Segments Custom Fields
 */
function cmb_engegroup_segments() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_engegroup_segments_';
    
    /**
    * Initiate the metabox
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => 'engegroup_segments',
            'title'         => __('Icons', 'engegroup'),
            'object_types'  => array('engegroup_segments'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    // Color Icon
    $cmb->add_field(
        array(
            'name'    => __('Icon (Color)', 'engegroup'),
            'desc'    => "",
            'id'      => $prefix .'icon_color',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'engegroup'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                            'image/svg+xml',
                    ),
            ),
            'preview_size' => array(225, 225) //'large', // Image size to use when previewing in the admin.
        ) 
    );

    // White Icon
    $cmb->add_field(
        array(
            'name'    => __('Icon (White)', 'engegroup'),
            'desc'    => "",
            'id'      => $prefix .'icon_white',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'engegroup'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                            'image/svg+xml',
                    ),
            ),
            'preview_size' => array(225, 225) //'large', // Image size to use when previewing in the admin.
        ) 
    );
}
add_action('cmb2_admin_init', 'cmb_engegroup_segments');

/**
 * Services
 */
function cpt_engegroup_services() 
{
    $labels = array(
            'name'                  => __('Services', 'engegroup'),
            'singular_name'         => __('Service', 'engegroup'),
            'menu_name'             => __('Services', 'engegroup'),
            'name_admin_bar'        => __('Service', 'engegroup'),
            'archives'              => __('Service Archives', 'engegroup'),
            'attributes'            => __('Service Attributes', 'engegroup'),
            'parent_item_colon'     => __('Parent Service:', 'engegroup'),
            'all_items'             => __('All Services', 'engegroup'),
            'add_new_item'          => __('Add New Service', 'engegroup'),
            'add_new'               => __('Add service', 'engegroup'),
            'new_item'              => __('New Service', 'engegroup'),
            'edit_item'             => __('Edit Service', 'engegroup'),
            'update_item'           => __('Update Service', 'engegroup'),
            'view_item'             => __('View Service', 'engegroup'),
            'view_items'            => __('View Services', 'engegroup'),
            'search_items'          => __('Search Service', 'engegroup'),
            'not_found'             => __('Not found', 'engegroup'),
            'not_found_in_trash'    => __('Not found in Trash', 'engegroup'),
            'featured_image'        => __('Featured Image', 'engegroup'),
            'set_featured_image'    => __('Set featured image', 'engegroup'),
            'remove_featured_image' => __('Remove featured image', 'engegroup'),
            'use_featured_image'    => __('Use as featured image', 'engegroup'),
            'insert_into_item'      => __('Insert into service', 'engegroup'),
            'uploaded_to_this_item' => __('Uploaded to this service', 'engegroup'),
            'items_list'            => __('Services list', 'engegroup'),
            'items_list_navigation' => __('Services list navigation', 'engegroup'),
            'filter_items_list'     => __('Filter services list', 'engegroup'),
    );
    $rewrite = array(
            'slug'                  => __('services', 'engegroup'),
            'with_front'            => false,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Service', 'engegroup'),
            'description'           => __('Engegroup Services', 'engegroup'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail',  'page-attributes'),
            'taxonomies'            => array(''),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 6,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 193 160"><g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"><path fill="black" stroke="#FFFFFF" d="M160.884 156.406h14.6V10.51a7.3 7.3 0 1 0-14.6 0v145.896zM146.784 43h42.797m-42.798 19h42.798M17.097 156.406h14.6V10.51a7.3 7.3 0 0 0-14.6 0v145.896zM3 43h42.798M3 26h186.68M3 62h42.798m30.456-37.256v-1.488a7.3 7.3 0 0 0-14.6 0v1.488m43.19 0v-1.488a7.3 7.3 0 1 0-14.6 0v1.488m43.187 0v-1.488a7.3 7.3 0 0 0-14.6 0v1.488M112 92.019c4.996-2.035 7.859-9.07 9.604-19.6 2.505-15.109 2.876-34.68 4.597-46.419M79.201 92.019c-4.996-2.035-7.859-9.07-9.604-19.6C67.092 57.31 66.72 37.739 65 26"/><path fill="black" stroke="#FFFFFF" d="M86.09 107.747V99.39a7.3 7.3 0 0 0-14.6 0v8.357m49.843 0V99.39a7.3 7.3 0 1 0-14.6 0v8.357m27.634 6.87a6.873 6.873 0 0 0-6.87-6.87H67.592a6.87 6.87 0 0 0-6.87 6.87v41.79h73.644v-41.79zM48.375 157h98.338"/></g></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('engegroup_services', $args); 
} 
add_action('init', 'cpt_engegroup_services', 0);


/**
 * Works
 */
function cpt_engegroup_works() 
{
    $labels = array(
            'name'                  => __('Works', 'engegroup'),
            'singular_name'         => __('Work', 'engegroup'),
            'menu_name'             => __('Works', 'engegroup'),
            'name_admin_bar'        => __('Work', 'engegroup'),
            'archives'              => __('Work Archives', 'engegroup'),
            'attributes'            => __('Work Attributes', 'engegroup'),
            'parent_item_colon'     => __('Parent Work:', 'engegroup'),
            'all_items'             => __('All Works', 'engegroup'),
            'add_new_item'          => __('Add New Work', 'engegroup'),
            'add_new'               => __('Add work', 'engegroup'),
            'new_item'              => __('New Work', 'engegroup'),
            'edit_item'             => __('Edit Work', 'engegroup'),
            'update_item'           => __('Update Work', 'engegroup'),
            'view_item'             => __('View Work', 'engegroup'),
            'view_items'            => __('View Works', 'engegroup'),
            'search_items'          => __('Search Work', 'engegroup'),
            'not_found'             => __('Not found', 'engegroup'),
            'not_found_in_trash'    => __('Not found in Trash', 'engegroup'),
            'featured_image'        => __('Featured Image', 'engegroup'),
            'set_featured_image'    => __('Set featured image', 'engegroup'),
            'remove_featured_image' => __('Remove featured image', 'engegroup'),
            'use_featured_image'    => __('Use as featured image', 'engegroup'),
            'insert_into_item'      => __('Insert into work', 'engegroup'),
            'uploaded_to_this_item' => __('Uploaded to this work', 'engegroup'),
            'items_list'            => __('Works list', 'engegroup'),
            'items_list_navigation' => __('Works list navigation', 'engegroup'),
            'filter_items_list'     => __('Filter Works list', 'engegroup'),
    );
    $rewrite = array(
            'slug'                  => __('works', 'engegroup'),
            'with_front'            => false,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Work', 'engegroup'),
            'description'           => __('Engegroup Works', 'engegroup'),
            'labels'                => $labels,
            'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 7,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 102 54"><g fill="none" fill-rule="nonzero" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"><path fill="none" stroke="black" d="M5.524 45.437C5.524 17.992 24.794 1 43.614 1c18.822 0 34.08 15.056 34.08 34.08 0 2.688 10.378 5.68 10.378 5.68l11.997 4.677s-12.33 5.044-39.13 1.707c-6.436-1.313-10.002-4.518-18.883-4.518-8.882 0-13.644 5.28-15.523 7.539-2.592 3.118-35.093 3.118-21.01-4.203v-.525zM26.186 13.26c5.994-4.143 12.866-6.37 19.678-6.37"/><path fill="none" stroke="black" d="M67.582 41.001a155.236 155.236 0 0 1-8.167-.804c-6.435-1.312-10.001-4.517-18.884-4.517-8.88 0-12.894 6.224-15.52 7.538"/><path fill="none" stroke="black" d="M39.504 21.407a3.415 3.415 0 1 1-6.826 0 3.415 3.415 0 0 1 6.826 0zm12.192 0a3.415 3.415 0 1 1-6.826 0 3.415 3.415 0 0 1 6.826 0zm12.19 0a3.415 3.415 0 1 1-6.826 0 3.415 3.415 0 0 1 6.827 0z"/></g></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('engegroup_works', $args); 
} 
add_action('init', 'cpt_engegroup_works', 0);

/**
 * Works Custom Fields
 */
function cmb_engegroup_works() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_engegroup_works_';
    
    /**
    * Initiate the metabox
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => 'engegroup_works',
            'title'         => __('Work', 'engegroup'),
            'object_types'  => array('engegroup_works'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    // Client logo
    $cmb->add_field(
        array(
            'name'    => __('Logo', 'engegroup'),
            'desc'    => __('Client logo', 'engegroup'),
            'id'      => $prefix .'logo',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'engegroup'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                            'image/svg+xml',
                    ),
            ),
            'preview_size' => array(225, 225) //'large', // Image size to use when previewing in the admin.
        ) 
    );

    // Local
    $cmb->add_field( 
        array(
            'name'       => __('Local', 'engegroup'),
            'desc'       => '',
            'id'         => $prefix . 'local',
            'type'       => 'text',
            //'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        )
    );

    //Date
    $cmb->add_field( 
        array(
            'name'       => __('Date', 'engegroup'),
            'desc'       => '',
            'id'         => $prefix . 'description', //Date 
            'type' => 'text',
            'attributes' => array(
                'type' => 'number',
                'pattern' => '\d*',
            ),
        ) 
    );

    //Segment
    $cmb->add_field( array(
        'name'           => __('Segment', 'engegroup'),
        'desc'           => '',
        'id'             => $prefix.'segment',
        'type'           => 'pw_select',
        'options'        => get_cmb_segments_array( array( 'post_type' => 'engegroup_segments' ) ),
    ));

    //Services group
    $engegroup_work_services_id = $cmb->add_field( array(
    'id'          => $prefix . 'services',
    'type'        => 'group',
    'description' => __( 'Services performed', 'engegroup' ),
    // 'repeatable'  => false, // use false if you want non-repeatable group
    'options'     => array(
        'group_title'   => __( 'Service {#}', 'engegroup' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Add Another Service', 'engegroup' ),
        'remove_button' => __( 'Remove Service', 'engegroup' ),
        'sortable'      => true, // beta
        // 'closed'     => true, // true to have the groups closed by default
        ),
    ) );

    //Service
    $cmb->add_group_field( 
        $engegroup_work_services_id, array(
            'name' => __('Service', 'engegroup'),
            'description' => '',
            'id'   => 'service',
            'type' => 'text',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_engegroup_works');

/**
 * Cases
 */
function cpt_engegroup_cases() 
{
    $labels = array(
            'name'                  => __('Cases', 'engegroup'),
            'singular_name'         => __('Case', 'engegroup'),
            'menu_name'             => __('Cases', 'engegroup'),
            'name_admin_bar'        => __('Case', 'engegroup'),
            'archives'              => __('Case Archives', 'engegroup'),
            'attributes'            => __('Case Attributes', 'engegroup'),
            'parent_item_colon'     => __('Parent Case:', 'engegroup'),
            'all_items'             => __('All Cases', 'engegroup'),
            'add_new_item'          => __('Add New Case', 'engegroup'),
            'add_new'               => __('Add case', 'engegroup'),
            'new_item'              => __('New Case', 'engegroup'),
            'edit_item'             => __('Edit Case', 'engegroup'),
            'update_item'           => __('Update Case', 'engegroup'),
            'view_item'             => __('View Case', 'engegroup'),
            'view_items'            => __('View Cases', 'engegroup'),
            'search_items'          => __('Search Case', 'engegroup'),
            'not_found'             => __('Not found', 'engegroup'),
            'not_found_in_trash'    => __('Not found in Trash', 'engegroup'),
            'featured_image'        => __('Featured Image', 'engegroup'),
            'set_featured_image'    => __('Set featured image', 'engegroup'),
            'remove_featured_image' => __('Remove featured image', 'engegroup'),
            'use_featured_image'    => __('Use as featured image', 'engegroup'),
            'insert_into_item'      => __('Insert into case', 'engegroup'),
            'uploaded_to_this_item' => __('Uploaded to this case', 'engegroup'),
            'items_list'            => __('Cases list', 'engegroup'),
            'items_list_navigation' => __('Cases list navigation', 'engegroup'),
            'filter_items_list'     => __('Filter Cases list', 'engegroup'),
    );
    $rewrite = array(
            'slug'                  => __('projects/cases', 'engegroup'),
            'with_front'            => false,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Case', 'engegroup'),
            'description'           => __('Engegroup Cases', 'engegroup'),
            'labels'                => $labels,
            'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'hierarchical'          => true,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 8,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65 62"><g fill="none"><path fill="none" stroke="black" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M61.612 1a2.349 2.349 0 0 1 2.348 2.348v36.637a2.349 2.349 0 0 1-2.348 2.348H3.348A2.349 2.349 0 0 1 1 39.985V3.348A2.349 2.349 0 0 1 3.348 1h58.264z"/><path fill="black" d="M23.631 39.751a6.406 6.406 0 1 1-12.81 0 6.406 6.406 0 0 1 12.81 0"/><path fill="white" stroke="black" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M23.631 39.751a6.406 6.406 0 1 1-12.81 0 6.406 6.406 0 0 1 12.81 0z"/><path fill="black" d="M5.307 60.122v-8.666a2.668 2.668 0 0 1 2.666-2.667h3.676l5.576 4.801 5.004-4.8h4.251a2.668 2.668 0 0 1 2.667 2.666v8.614l-23.84.052z"/><path fill="black" stroke="black" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.307 60.122v-8.666a2.668 2.668 0 0 1 2.666-2.667h3.676l5.576 4.801 5.004-4.8h4.251a2.668 2.668 0 0 1 2.667 2.666v8.614l-23.84.052z"/><path fill="black" d="M54.138 39.751a6.406 6.406 0 1 1-12.811 0 6.406 6.406 0 0 1 12.811 0"/><path fill="white" stroke="black" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M54.138 39.751a6.406 6.406 0 1 1-12.811 0 6.406 6.406 0 0 1 12.811 0z"/><path fill="black" d="M35.813 60.122v-8.666a2.668 2.668 0 0 1 2.667-2.667h3.676l5.576 4.801 5.004-4.8h4.25a2.668 2.668 0 0 1 2.667 2.666v8.614l-23.84.052z"/><path fill="black" stroke="black" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M35.813 60.122v-8.666a2.668 2.668 0 0 1 2.667-2.667h3.676l5.576 4.801 5.004-4.8h4.25a2.668 2.668 0 0 1 2.667 2.666v8.614l-23.84.052z"/><path fill="none" stroke="black" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M26.206 20.72l4.497 1.725S28.991 34.113 29.1 34.011l9.655-12.504-4.683-1.416.796-10.768-8.661 11.397z"/></g></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('engegroup_cases', $args); 
} 
add_action('init', 'cpt_engegroup_cases', 0);

/**
 * Cases Custom Fields
 */
function cmb_engegroup_cases() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_engegroup_cases_';
    
    /**
    * Initiate the metabox
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => 'engegroup_cases',
            'title'         => __('Case', 'engegroup'),
            'object_types'  => array('engegroup_cases'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    // Client logo
    $cmb->add_field(
        array(
            'name'    => __('Logo (Color)', 'engegroup'),
            'desc'    => "",
            'id'      => $prefix .'logo_color',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'engegroup'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                            'image/svg+xml',
                    ),
            ),
            'preview_size' => array(225, 225) //'large', // Image size to use when previewing in the admin.
        ) 
    );

    // Client logo
    $cmb->add_field(
        array(
            'name'    => __('Logo (White)', 'engegroup'),
            'desc'    => "",
            'id'      => $prefix .'logo_white',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'engegroup'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                            'image/svg+xml',
                    ),
            ),
            'preview_size' => array(225, 225) //'large', // Image size to use when previewing in the admin.
        ) 
    );

    // Local
    $cmb->add_field( 
        array(
            'name'       => __('Local', 'engegroup'),
            'desc'       => '',
            'id'         => $prefix . 'local',
            'type'       => 'text',
            //'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        )
    );

    //Date
    $cmb->add_field( 
        array(
            'name'       => __('Date', 'engegroup'),
            'desc'       => '',
            'id'         => $prefix . 'description',
            'type' => 'text',
            'attributes' => array(
                'type' => 'number',
                'pattern' => '\d*',
            ),
        ) 
    );

    //Segment
    $cmb->add_field( array(
        'name'           => __('Segment', 'engegroup'),
        'desc'           => '',
        'id'             => $prefix.'segment',
        'type'           => 'pw_select',
        'options'        => get_cmb_segments_array( array( 'post_type' => 'engegroup_segments' ) ),
    ));
    
    //Services group
    $engegroup_work_services_id = $cmb->add_field( array(
    'id'          => $prefix . 'services',
    'type'        => 'group',
    'description' => __( 'Services performed', 'engegroup' ),
    // 'repeatable'  => false, // use false if you want non-repeatable group
    'options'     => array(
        'group_title'   => __( 'Service {#}', 'engegroup' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Add Another Service', 'engegroup' ),
        'remove_button' => __( 'Remove Service', 'engegroup' ),
        'sortable'      => true, // beta
        // 'closed'     => true, // true to have the groups closed by default
        ),
    ) );
    
    //Service
    $cmb->add_group_field( 
        $engegroup_work_services_id, array(
            'name' => __('Service', 'engegroup'),
            'description' => '',
            'id'   => 'service',
            'type' => 'text',
        )
    );

    //Photos
    $cmb->add_field(
        array(
            'name'    => __('Photos', 'engegroup'),
            'desc'    => '',
            'id'      => $prefix .'photos',
            'type'    => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'engegroup'), // Change upload button text. Default: "Add or Upload File"
                    'remove_image_text' => __('Remove Image', 'engegroup'), // default: "Remove Image"
                    'file_text' => __('File', 'engegroup'), // default: "File:"
                    'file_download_text' => __('Download', 'engegroup'), // default: "Download"
                    'remove_text' => __('Remove', 'engegroup'), // default: "Remove"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(225, 127) //'large', // Image size to use when previewing in the admin.
        ) 
    );
}
add_action('cmb2_admin_init', 'cmb_engegroup_cases');

?>